### **Requirements**
* Python 3.8
* pip3

### **How to setup?**
To setup the project you need to run "setup.sh" bash script. It installs all needed python libraries.
```bash
chmod +x setup.sh
./setup.sh
```

### **How to run?**
* To run all tasks at once run "run.sh" bash script.
```bash
chmod +x run.sh
./run.sh
```

* If you want to run each task separately type in console
```bash
ipython <task_name>.py
```
OR  
run jupyter lab/notebook and run each task in this environment