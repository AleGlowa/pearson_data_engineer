#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd

get_ipython().run_line_magic('store', '-r first_dataset second_dataset DATA_DIR')


# In[2]:


# Save first final dataset to csv format with ',' seperator
first_dataset.to_csv(DATA_DIR / 'test_utilization.csv', sep=',', index=False)

# Save second final dataset to csv format with ',' seperator
second_dataset.to_csv(DATA_DIR / 'test_average_scores.csv', sep=',', index=False)


# In[ ]:




