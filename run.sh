#!/usr/bin/env bash

for task_number in 1 2 3 4 5 6; do
    COMMAND="ipython Task${task_number}.py"
    echo "COMMAND=${COMMAND}"
    eval ${COMMAND}
done
echo
echo

echo "Done ./run.sh!"