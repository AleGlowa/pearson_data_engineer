#!/usr/bin/env python
# coding: utf-8

# In[7]:


import pandas as pd

get_ipython().run_line_magic('store', '-r df_test df_class df_test_level')


# In[8]:


# Delete the rows where `created_at`column is greater than `updated_at` column
# Delete the rows where student started solving test before its creation datetime
# Delete the rows where `last_event_time` column is smaller than `created_at` column
# Presume that `authorized_at` column is an event time on test, so that column cannot be greater than ..
# .. `last_event_time` column
df_test = df_test.loc[~(df_test['created_at'] > df_test['updated_at']) &
                      (~(df_test['authorized_at'] < df_test['created_at']) | df_test['authorized_at'].isnull()) &
                      (~(df_test['last_event_time'].astype('datetime64[ns]') < df_test['created_at']) | df_test['last_event_time'].isnull()) &
                      (~(df_test['last_event_time'].astype('datetime64[ns]') < df_test['authorized_at']) | df_test['last_event_time'].isnull() | df_test['authorized_at'].isnull())]


## Delete the rows where average from all 4 scores isn't equal to column `overall_score`
means_by_scores = round(df_test[['speaking_score', 'writing_score', 'reading_score', 'listening_score']].mean(axis=1))
overall_scores = df_test['overall_score']

# NaN values also count as True (NaN == NaN evaluates to True)
is_equal = (means_by_scores == overall_scores) | (means_by_scores.isnull() & overall_scores.isnull())
df_test = df_test.loc[is_equal]

## Delete the rows where creation date of test is smaller than creation date of test's level
# Preserve order of index from df_test
merge_test_level = df_test.reset_index().merge(df_test_level, left_on='test_level_id', right_on='id', suffixes=('_test', '_level')).set_index('id')
is_smaller = (merge_test_level['created_at_test'] < merge_test_level['created_at_level'])
df_test = df_test.loc[~is_smaller]

## Delete the rows where creation date of test is smaller than creation date of class
# Preserve order of index from df_test
merge_class = df_test.reset_index().merge(df_class, left_on='class_id', right_on='id', suffixes=('_test', '_class')).set_index('id')
is_smaller = (merge_class['created_at_test'] < merge_class['created_at_class'])
df_test = df_test.loc[~is_smaller]

# 'Compressed' `df_test` dataframe to 4624 rows from 13039 rows, it gives us about 35% of original data


# In[9]:


get_ipython().run_line_magic('store', 'df_test df_class df_test_level')


# In[ ]:




