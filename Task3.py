#!/usr/bin/env python
# coding: utf-8

# In[16]:


import pandas as pd

get_ipython().run_line_magic('store', '-r df_test df_class')


# In[17]:


# Temp dataframe with scored tests
df_scored = df_test.loc[(df_test['test_status'] == 'SCORING_SCORED') & ~df_test['authorized_at'].isnull()]

# Column mapping
column_map = {'name': 'class_name', 'id': 'test_id', 'test_level_id': 'test_level', 'created_at': 'test_created_at', 'authorized_at': 'test_authorized_at'}


# In[18]:


first_dataset = df_scored.reset_index()[['class_id', 'id', 'test_level_id', 'created_at', 'authorized_at']].merge(df_class[['name', 'teaching_hours']], left_on='class_id', right_index=True)

# Change column names
first_dataset.rename(columns=column_map, inplace=True)

# Change the order of the columns
first_dataset = first_dataset.reindex(['class_id', 'class_name', 'teaching_hours', 'test_id', 'test_created_at', 'test_authorized_at', 'test_level'], axis=1)

# Add additional column `class_test_number` indicating how many students solved tests
class_test_number = df_scored.reset_index().groupby(['class_id', 'id']).count()['student_id'].groupby(level=0).cumsum().values
first_dataset['class_test_number'] = class_test_number


# In[19]:


get_ipython().run_line_magic('store', 'df_scored column_map first_dataset')


# In[ ]:




