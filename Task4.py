#!/usr/bin/env python
# coding: utf-8

# In[11]:


import pandas as pd

get_ipython().run_line_magic('store', '-r df_scored df_class column_map')


# In[12]:


second_dataset = df_class[['name', 'teaching_hours']].merge(df_scored[['class_id', 'created_at', 'authorized_at']], left_index=True, right_on='class_id')

# Change column names
second_dataset.rename(columns=column_map, inplace=True)

# Change the order of the columns
second_dataset = second_dataset.reindex(['class_id', 'class_name', 'teaching_hours', 'test_created_at', 'test_authorized_at'], axis=1)

# Drop duplicates to calculate average score for each class (keep the oldest `test_created_at` date)
second_dataset.drop_duplicates(subset='class_id', keep='first', inplace=True)

# Add additional column `avg_class_test_overall_score` indicating average `overall_score` for each class
avg_class_test_overall_score = round(df_scored.groupby('class_id').mean()['overall_score'], 1).values
second_dataset['avg_class_test_overall_score'] = avg_class_test_overall_score


# In[13]:


get_ipython().run_line_magic('store', 'second_dataset')


# In[ ]:




