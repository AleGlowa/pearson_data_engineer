#!/usr/bin/env python
# coding: utf-8

# In[1]:


import urllib.parse                   # for encrypting password for access to database

import psycopg2                       # for connecting to database
from sqlalchemy import create_engine  # for connecting to database

get_ipython().run_line_magic('store', '-r first_dataset second_dataset')


# In[2]:


# Encrpyting password
encrypted_pass = urllib.parse.quote_plus('password')

# Create a connection to database
conn = psycopg2.connect(host='localhost', port = 5432, database='database', user='user', password=f'{encrypted_pass}')

# Create a cursor object from connection object
cur = conn.cursor()

# Save two datasets as tables in PostgreSQL database
engine = create_engine(f'postgresql+psycopg2://user:{encrypted_pass}@localhost:5432/database')
first_dataset.to_sql('Test_utilization', engine)
second_dataset.to_sql('Test_average_scores', engine)

# Close the cursor and connection so the server can allocate bandwidth to other requests
cur.close()
conn.close()


# In[4]:


get_ipython().run_line_magic('reset', '')


# In[ ]:




