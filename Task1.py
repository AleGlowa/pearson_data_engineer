#!/usr/bin/env python
# coding: utf-8

# In[11]:


from pathlib import Path              # for reading datasets

import pandas as pd

DATA_DIR = Path('data')


# In[12]:


df_class = pd.read_csv(DATA_DIR / 'class.csv', sep=';', header=0, index_col='id', parse_dates=['created_at', 'updated_at', 'latest_test_time'])
df_test_level = pd.read_csv(DATA_DIR / 'test_level.csv', sep=';', header=0, index_col='id', parse_dates=['created_at', 'updated_at'])
df_test = pd.read_csv(DATA_DIR / 'test.csv', sep=';', header=0, index_col='id', parse_dates=['created_at', 'updated_at', 'last_event_time', 'authorized_at'])


# In[13]:


get_ipython().run_line_magic('store', 'df_class df_test_level df_test DATA_DIR')


# In[ ]:




